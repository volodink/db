---
marp: true
theme: lection
paginate: true
size: 16:9
---

<!-- _class: lead -->
<!-- _paginate: false -->

# Управление данными

## Введение в базы данных

### Термины и определения

---

# Основные материалы

Курс построен на материалах следующих ресурсов:

1. Курс профессора Стенфорда Jennifer Widom https://www.classcentral.com/course/db-303
2. Курсы на текущий момент https://cs.stanford.edu/people/widom/DB-mooc.html
   ![bg left:49%](https://www.paloaltoonline.com/news/photos/2013/may/10/30518_original.jpg)

---

# Introduction to Structured Query Language (SQL)

Charles Severance

University of Michigan

https://www.classcentral.com/course/db-303

![bg left](https://akm-img-a-in.tosshub.com/sites/btmt/images/stories/dr-chuck_660_111418101124.jpg)

---

# Информационные ресурсы: online-курсы

1. Introduction to Relational Databases https://www.coursera.org/learn/introduction-to-relational-databases#syllabus

2. Databases and SQL for Data Science with Python https://www.coursera.org/learn/sql-for-data-science#syllabus

![](https://d3njjcbhbojbot.cloudfront.net/api/utilities/v1/imageproxy/http://coursera-university-assets.s3.amazonaws.com/bb/f5ced2bdd4437aa79f00eb1bf7fbf0/IBM-Logo-Blk---Square.png?auto=format%2Ccompress&dpr=1&w=120&h=120&q=40)

---

# Database Management System (DBMS) provides….

... efficient, reliable, convenient, and safe

multi-user storage of and access to massive

amounts of persistent data.

---

# DBMS

1. Massive => _terabytes_
2. Persistent => _long time period_
3. Safe => _hardware, software, power, users_
4. Multi-user => _concurency control_
5. Convenient
   - _physical data independence_
   - _high-level query language_ => _declarative_
6. Efficient
   - _thousands of queries/update per second_
7. Reliable
   - 99.99999%

---

# DBMS

1. Database applications may be programmed via “frameworks”

2. DBMS may run in conjunction with “middleware”

3. Data-intensive applications may not use DBMS at all (_DBMS itself_)

---

# Key concepts

1. Data model => _set of records_
   - _xml_
   - _graph_
   - _json_
2. Schema versus data
   - _types_
   - _variables_
3. Data definition language (DDL)
   - _setup schema_
4. Data manipulation or query language (DML)
   - _quering and modifying_

---

# Key people

1. DBMS implementer
   - _buids system_
2. Database designer
   - _establishes schema_
3. Database application developer
   - _programs that operate on database_
4. Database administrator
   - _loads data, keeps running smoothly_

---

# Whether you know it or not, you’re using a database.

![w:500px](img/db.png)

---

# Информационные ресурсы: online-курсы

0. PostgreSQL tutorial - https://www.postgresqltutorial.com/
1. Курсы по SQL на [Stepik](https://stepik.org/catalog/search?q=sql)
2. Postgrespro образование - https://postgrespro.ru/education

- https://postgrespro.ru/education/courses/DBA1

3. w3schools SQL - https://www.w3schools.com/sql/default.asp
4. SQL. База видеокурсов - https://vk.com/videos-54530371?section=album_56085487

5. Серия коротких видео по основам SQL - https://vk.com/videos-54530371?section=album_56085800

\*список в процессе дополнения

---

<!-- _class: manylines3 -->

# Информационные ресурсы: документация

1. ![h:48px](https://postgrespro.ru/img/apple-touch-icon-57x57.png) PostgreSQL в России - https://postgrespro.ru/
2. ![h:48px](https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/Postgresql_elephant.svg/1200px-Postgresql_elephant.svg.png) Официальный сайт PostgreSQL - https://www.postgresql.org/
3. ![h:48px](https://www.db-fiddle.com/favicon-7f8b8049f6e2ae243dcd50e3925fe458.png) DB Fiddle (SQL Database Playground) - https://www.db-fiddle.com/
4. PlantUML - https://plantuml.com/

- https://habr.com/ru/post/416077/

5. pgAdmin PostgreSQL Tools - https://www.pgadmin.org/
6. PonyORM - https://ponyorm.org/
7. Web DSL 4 ER - https://dbdiagram.io/

- dbdiagram.io tutorial https://hackernoon.com/dbdiagram-io-a-database-diagram-designer-built-for-developers-and-analysts-975f310d4f13

8. dBeaver - https://dbeaver.io/ ![w:48px h:48x](https://dbeaver.io/wp-content/uploads/2015/09/beaver-head.png)

---

# Ссылки на ресурсы

1. Эта лекция: [тут](https://volodink.gitlab.io/db/db_intro.pdf)
2. Репозиторий с лекциями: https://gitlab.com/volodink/db
3. Материалы для 2 курса: [gdrive](https://drive.google.com/drive/folders/15TxXASnOwhOqVp1MCxzL8S3y_pXkFYSl)
4. Курс в Moodle: [тут](https://edu.penzgtu.ru/course/view.php?id=11)

---

<!-- _class: oneline -->
<!-- _class: lead -->
<!-- _paginate: false -->

# Благодарю за внимание!

## Вопросы?
