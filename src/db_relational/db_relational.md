---
marp: true
theme: lection
paginate: true
size: 16:9
---

<!-- _class: lead -->
<!-- _paginate: false -->

# Управление данными

## Relational

---

# Relational databases



---

# Ссылки на ресурсы

1. Эта лекция: [тут](https://volodink.gitlab.io/db/db_relational.pdf)
2. Репозиторий с лекциями: https://gitlab.com/volodink/db
3. Материалы для 2 курса: [gdrive](https://drive.google.com/drive/folders/15TxXASnOwhOqVp1MCxzL8S3y_pXkFYSl)
4. Курс в Moodle: [тут](https://edu.penzgtu.ru/course/view.php?id=11)

---

<!-- _class: oneline -->
<!-- _class: lead -->
<!-- _paginate: false -->

# Благодарю за внимание!

## Вопросы?
