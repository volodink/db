---
marp: true
theme: lection
paginate: true
size: 16:9
---

<!-- _class: lead -->
<!-- _paginate: false -->

# Управление данными

## Введение
## Общие вопросы, постановка задачи

---

<!-- _class: contacts -->

# Контакты

![w:80px left](img/gmail-logo.svg) volodin.konstantin@gmail.com

![w:80px left](img/vk-logo.svg) vk.com/volodin.konstantin

![w:80px left](img/telegram-logo.svg) @volodink

---

<!-- _class: invert -->
<!-- _paginate: false -->

# <!-- fit -->TL;DR

---

# Виды активностей

1. Аудиторные

- Лекции
- Лабораторные

2. Внеаудиторные

- Самостоятельная работа (дз, подготовка контрольных и КР)\*
- Консультации по вопросам\*
- Семинары\*

_\*согласоввываются индивидуально_

---

# План работ

**3 семестр**

1. Лекции - 16 часов => 1 раз в неделю, 45 мин :house: :sleeping:

2. Лаборные работы - 36 часов => 1 раз в неделю, 2\*45 мин :scream:

3. Контрольная работа :scream: :scream:

4. Курсовая  :underage: :scream: :scream: :scream:

5. Семинары\* :rocket:

6. Внезапная проверка теоретических / готовности писать запросы / остатков / останков знаний :sos:

_\*по согласованию, специальные и углубленные вопросы_

---

# Связь дисциплин

1. Системное программирование
2. Языки программрования
3. Алгоритмизация и программирование
4. ООП
5. Современные технологии разработки программного обеспечения

![bg right](https://asdela.ru/wp-content/uploads/2015/08/IMG_3650.jpg)

---
<!-- _class: invert -->
<!-- _paginate: false -->

# <!-- fit -->SQL

---
<!-- _paginate: false -->

![bg 90%](https://i.ytimg.com/vi/dLOFKsB2a04/maxresdefault.jpg)

---

# Почему PostgreSQL ?

![bg w:1100px](img/postgrespro.png) 

---
<!-- _class: contacts -->

# Основные технологические отличия

:rocket: Проверка знаний на платформе  https://contest.yandex.ru
- автоматизированная проверка формализованных задач;
- написание кода для решения задачи;
- массовость.

# Основные концептуальные отличия

:fire:Лабораторные работы == тренинги и демонстрации для активного усвоения с учителем
:fire::fire: Прошел часть - реализуй часть КР !!!

---

# 3 семестр

**Лекции:** _основы, общие понятия,  SQL basics_

1. Введение
2. Теория: множества, кортежи, ACID, теория графов, деревья, B-tree
3. Проектирование БД: нормальные формы, аномалии, ограничения и т.п.
4. Проектирование БД: правила Кодда
5. Проектирование БД: ER-диаграммы
6. Основы SQL
7. Основы администрирования:
- PostgreSQL: psql, конфиги; pgAdmin; vagrant, bash-скрипты

---

# 3 семестр

**Лабораторные работы** => тренинги и демонстрации

1.  Разработка концепции БД

2.  Разработка схемы БД

3.  Работа с EA над ER-диаграммой

4.  SELECT

5.  UPDATE, DELETE, INSERT

6.  Index: create, alter, delete

7.  VIEW
...


---

# 3 семестр

**Контрольная работа** => семестровое задание

1.  Запросы на языке реляционный алгебры
2. _задание загружается ...._

---
# 2 семестр
**Курсовая работа** => целостный проект
:book: Прошел тему на практике - сделай для своей темы в КР
:book: Разработка БД без написания ПО... ну или почти :) 
1. Цель КР: систематизация знаний по вопросам проектирования БД и ее реализации с применением SQL
2. Предметную область выбирает студент => формируется ТЗ
3. Можно выбрать уровень освоения: 5, 4, 3
4. Выполняется в течении прохождения 3-го семестра
5. Защита курсовой работы с оценкой - лето 2021 года

:zap: НЕ пропустите отдельный семинар !
:zap: Ответственный преподаватель: Пискаев К.Ю.

---

# Информационные ресурсы: иностранные языки

1. Словарь английского/курсы
2. ~~Google translate~~
3. Мультитран: http://www.multitran.ru/c/m.exe?a=1&SHL=2

![](https://upload.wikimedia.org/wikipedia/en/thumb/1/14/Concise_Oxford_English_Dictionary.jpg/220px-Concise_Oxford_English_Dictionary.jpg) ![h:330px](https://img4.labirint.ru/rc/cd3a130fc81abcb4d2a4ebbde1f55a8f/220x340/books67/667170/cover.jpg?1564144853) ![h:330px](img/eng.jpg)

---

<!-- _class: manylines2 -->

# Информационные ресурсы: книги

1. Администрирование PostgreSQL 9. Книга рецептов | Ханну Кросинг, Саймон Ригс
2. PostgreSQL. Основы | Ричард Стоунз, Нейл Мэттью
3. Энтони Молинаро | "SQL. Сборник рецептов"
4. Изучаем SQL. | Алан Бьюли
5. Эрик Редмонд, Джим Р. Уилсон | "Семь баз данных за семь недель. Введение в современные базы данных и идеологию NoSQL"

![w:200px h:280px](https://progerlib.ru/wp-content/uploads/2020/05/administrirovanie-postgresql-9.png) ![w:200px h:280px](img/31.jpg) ![w:200px h:280px](https://media.proglib.io/wp-uploads/2017/04/26vOS1bLTgM.jpg) ![w:200px h:280px](https://progerlib.ru/wp-content/uploads/2020/06/izuchaem-sql.png) ![w:200px h:280px](https://library-it.com/wp-content/uploads/2020/11/redmond_je_uilson_d_sem_baz_dannyh.jpg)

---

<!-- _class: manylines2 -->

# Информационные ресурсы: книги

1. Пол Уилтон | SQL для начинающих WROX Press

2. От хранения данных к управлению информацией | В.В. Смородин, Е.В. Волкова, А.А. Алиев Питер

3. Создаем динамические веб-сайты с помощью PHP, MySQL и JavaScript | Робин Никсон Серия: Бестселлеры O'Reilly Питер

![w:200px h:280px](img/pw_sql.jpeg) ![w:200px h:280px](img/dm.jpeg) ![w:200px h:280px](img/misc_sql.jpeg)

---

# Информационные ресурсы: online-курсы

1. Introduction to Relational Databases https://www.coursera.org/learn/introduction-to-relational-databases#syllabus 

2. SQL for Data Science https://www.coursera.org/learn/sql-for-data-science#syllabus

---

# Информационные ресурсы: online-курсы

0. PostgreSQL tutorial - https://www.postgresqltutorial.com/
1. Курсы по SQL на [Stepik](https://stepik.org/catalog/search?q=sql)
2. Postgrespro образование - https://postgrespro.ru/education
- https://postgrespro.ru/education/courses/DBA1
3. w3schools SQL - https://www.w3schools.com/sql/default.asp
4. SQL. База видеокурсов - https://vk.com/videos-54530371?section=album_56085487

5. Серия коротких видео по основам SQL - https://vk.com/videos-54530371?section=album_56085800

\*список в процессе дополнения

---
<!-- _class: manylines3 -->
# Информационные ресурсы: документация

1. ![h:48px](https://postgrespro.ru/img/apple-touch-icon-57x57.png) PostgreSQL в России  - https://postgrespro.ru/
2. ![h:48px](https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/Postgresql_elephant.svg/1200px-Postgresql_elephant.svg.png) Официальный сайт PostgreSQL - https://www.postgresql.org/
3. ![h:48px](https://www.db-fiddle.com/favicon-7f8b8049f6e2ae243dcd50e3925fe458.png) DB Fiddle (SQL Database Playground) - https://www.db-fiddle.com/
4. PlantUML - https://plantuml.com/
- https://habr.com/ru/post/416077/
5. pgAdmin PostgreSQL Tools - https://www.pgadmin.org/
6. PonyORM - https://ponyorm.org/
7. Web DSL 4 ER - https://dbdiagram.io/
- dbdiagram.io tutorial https://hackernoon.com/dbdiagram-io-a-database-diagram-designer-built-for-developers-and-analysts-975f310d4f13
8. dBeaver - https://dbeaver.io/ ![w:48px h:48x](https://dbeaver.io/wp-content/uploads/2015/09/beaver-head.png) 

---
<!-- _class: invert -->
<!-- _paginate: false -->

![bg w:1170px](https://adamchester.github.io/EaToSql/img/ea_sample_model.png)

---

# Gitlab

Хранение кода и автоматизация: gitlab.com

![width:1000px](img/gitlab-ci-logo.jpg)

---
<!-- _paginate: false -->

![bg](https://resources.jetbrains.com/storage/products/jetbrains/img/meta/preview.png)

---
<!-- _paginate: false -->

![bg](https://resources.jetbrains.com/storage/products/datagrip/img/meta/datagrip_1280x800.png)

---
<!-- _class: invert -->
<!-- _paginate: false -->

![bg w:1100px](https://images.g2crowd.com/uploads/attachment/file/72412/expirable-direct-uploads_2F2cd52e53-1f20-4b80-8929-ea79c7345352_2Fquery-console.png)

---
<!-- _class: invert -->
<!-- _paginate: false -->

![bg w:1070px](https://www.pgadmin.org/static/COMPILED/assets/img/screenshots/pgadmin4-explain.png)

---
<!-- _class: invert -->
<!-- _paginate: false -->

![bg w:1280px](https://dbeaver.io/wp-content/uploads/2018/03/data_edit.png)

---

# Лицензионное программное обеспечение

1. Зарегистрироваться на сайте JetBrains: https://account.jetbrains.com/login
2. Прислать мне сообщение с данной почты и текстом:
   _Я студент группы ХХХ, ФИО, хочу лицензию JetBrains_
   на мою почту (почта есть в контактах)

---

# Ссылки на ресурсы

1. Эта лекция: [тут](https://volodink.gitlab.io/db/intro.pdf)
2. Репозиторий с лекциями: https://gitlab.com/volodink/db
3. Материалы для 2 курса: [gdrive](https://drive.google.com/drive/folders/15TxXASnOwhOqVp1MCxzL8S3y_pXkFYSl)
4. Курс в Moodle: [тут](https://edu.penzgtu.ru/course/view.php?id=11)


---

<!-- _class: oneline -->
<!-- _class: lead -->
<!-- _paginate: false -->

# Благодарю за внимание!

## Вопросы?

