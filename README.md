### Сборник материалов по дисциплине "Управление данными"

Список лекций:

1. Введение - [[ссылка](https://volodink.gitlab.io/db/intro.pdf)]

2. Введение в БД - [[ссылка](https://volodink.gitlab.io/db/db_intro.pdf)]

3. Реляционные БД - [[ссылка](https://volodink.gitlab.io/db/db_relational.pdf)]

Полезные ссылки:

1. SSH клиент PuTTY для Windows: https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html

### Руководство разработчика

- TBD
