all:
	marp src/intro/intro.md -o build/intro.pdf
	marp src/db_intro/db_intro.md -o build/db_intro.pdf
	marp src/db_relational/db_relational.md -o build/db_relational.pdf

live:
	marp -p --server ./src/db_relational

ci:
	gitlab-runner exec shell build

bootstrap:
	echo "Installing VS Code"

clean:
	rm -rf build
	rm -rf builds
